'use strict';

// Set folder names
var rootFolder = process.cwd() + "/";
var src = rootFolder + 'src/',
    lib = rootFolder + "src/lib/",
    models = rootFolder + "src/models/";
//
//
// Load packages
const
    download = require("image-downloader"),
    db = require(lib + "mongoose"),
    async = require("async"),
    path = require("path"),
    //convertPdf2Png = require(lib + "convertPdf2Png"),
    kafkaConsumer = require(lib + "kafkaConsumer"),
    kafkaProducer=require(lib + "kafkaProducer"),
    downloader = require(lib + "downloader"),
    redownloader = require(lib + "redownloader"),
    logWriter = require(lib + "logWriter");

// Load models
//var Config = require(models + "config");
//var Order = require(models + "order");
//var ObjectId = require('mongoose').Types.ObjectId;


//kafkaProducer("order-blanket-fy2", "test123", "6b810236466", false, "this is a test");  
run();

function run()
{
    hourlyCheck() 
    kafkaConsumer();
    redownloader();
    setTimeout(function(){downloader();}, (60*1000));
}

function hourlyCheck()
{
    logWriter("-", "-", "CYCLE-CHECK", "-----------HOURLY-CHECK---------");
    setTimeout(function(){hourlyCheck();}, (60*60*1000));
}








