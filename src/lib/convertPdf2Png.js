// //
// var src = process.cwd() + '/src/';

// //
// const assert = require("assert"),
//   Canvas = require("canvas"),
//   fs = require("fs"),
//   pdfjsLib = require('pdfjs-dist'),
//   // log = require(libs + 'log')(module),
//   config = require(src + 'config');


// function NodeCanvasFactory() {}
// NodeCanvasFactory.prototype = {
//   create: function NodeCanvasFactory_create(width, height) {
//     assert(width > 0 && height > 0, 'Invalid canvas size');
//     var canvas = new Canvas(width, height);
//     var context = canvas.getContext('2d');
//     return {
//       canvas: canvas,
//       context: context,
//     };
//   },

//   reset: function NodeCanvasFactory_reset(canvasAndContext, width, height) {
//     assert(canvasAndContext.canvas, 'Canvas is not specified');
//     assert(width > 0 && height > 0, 'Invalid canvas size');
//     canvasAndContext.canvas.width = width;
//     canvasAndContext.canvas.height = height;
//   },

//   destroy: function NodeCanvasFactory_destroy(canvasAndContext) {
//     assert(canvasAndContext.canvas, 'Canvas is not specified');

//     // Zeroing the width and height cause Firefox to release graphics
//     // resources immediately, which can greatly reduce memory consumption.
//     canvasAndContext.canvas.width = 0;
//     canvasAndContext.canvas.height = 0;
//     canvasAndContext.canvas = null;
//     canvasAndContext.context = null;
//   },
// };

// //
// // Main entry point: convertPdf2Png
// //
// function convertPdf2Png(pdfPath, outputPreviewPath) {
//   return new Promise(function (resolve, reject) {

//     // Read the PDF file into a typed array so PDF.js can load it.
//     //var rawData = new Uint8Array(fs.readFileSync(pdfUrl));
//     //nativeImageDecoderSupport: 'none'
//     //nativeImageDecoderSupport
//     // Load the PDF file.
//     pdfjsLib.getDocument({
//       url: pdfPath,
//       nativeImageDecoderSupport: 'none',
//     })
//     //pdfjsLib.getDocument(rawData)
//       .then(function (pdfDocument) {
        
//         //console.log('# PDF document loaded.');

//         // Get the first page.
//         pdfDocument.getPage(1)
//           .then(function (page) {
//             // Render the page on a Node canvas with 100% scale.
//             var viewport = page.getViewport(1.0);
//             var canvasFactory = new NodeCanvasFactory();
//             var canvasAndContext = canvasFactory.create(viewport.width, viewport.height);
//             var renderContext = {
//               canvasContext: canvasAndContext.context,
//               viewport: viewport,
//               canvasFactory: canvasFactory
//             };

//             page.render(renderContext)
//               .then(function () {
//                 // Convert the canvas to an image buffer.
//                 var image = canvasAndContext.canvas.toBuffer();
//                 fs.writeFile(outputPreviewPath, image, function (error) {
//                   if (error) {
//                   // console.error('Error: ' + error);
//                   } else {
//                  //   console.log(canvasAndContext.canvas.toDataURL('image/jpeg'));
//                  //  console.log('Finished converting first page of PDF file to a PNG image.');
//                     resolve(true);
//                   }
//                 });
//               });
//           });
//       }).catch(function (reason) {
//       //  console.log(reason);
//         reject(reason);
//       });
//   }); // Promise
// } // convertPdf2Png
// //


// //
// module.exports = convertPdf2Png;