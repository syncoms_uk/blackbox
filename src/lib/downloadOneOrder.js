'use strict';
//
// Set folder names
const
    rootFolder = process.cwd() + "/";
var src = rootFolder + 'src/',
    lib = rootFolder + "src/lib/",
    models = rootFolder + "src/models/";
   


    
// downloader settings
var config = require(src + "config");
var facilityId = config.get("facility:id");
var dltempPath = config.get("downloader:tempPath");
var dlprintPath = config.get("downloader:printPath");
var dlretryInterval1 = config.get("downloader:retryIntervalSeconds_1"); 
var dlretryInterval2 = config.get("downloader:retryIntervalSeconds_2"); 
var dlretryInterval3 = config.get("downloader:retryIntervalSeconds_3");
var mwareTimeout = config.get("downloader:mwareTimeout");

var kafkaProducerTopic_downloadComplete= config.get("kafkaProducer-status:topic-download-complete");

// Load packages
var
    os = require('os'),
    mv = require("mv"),
    download = require('image-downloader'),
    fileExists = require('file-exists'),
    kafka = require("kafka-node"),
    makeDir = require('make-dir'),
    path = require("path"),
    kafkaProducer = require(lib + "kafkaProducer"),
    logWriter = require(lib + "logWriter"),
    logdate = require(lib + "logdate"),
    downloadOneSn = require(lib + "downloadOneSn");
    

//
//Load models
var Order = require(models + "order");

function downloadOneOrder(sourceOrderId){
    return new Promise(function (resolve, reject) {
        var query = {"sourceOrderId": sourceOrderId};
        var orderExists=false;
        Order.findOne(query, function(err, order) { 
            if(err)
                logWriter(t.sourceOrderId, '-', "downloadOneOrder", "Error (Order.FindOne): " + err);

            if(order==null){
                    resolve({sourceOrderId: "null", result:false, message:"order==null"});
                    return;
            }
            if(order.isDownloaded){
                for (var i = 0; i < order.items.length; i++) {
                    kafkaProducer_StatusDownloadCompele(order.sourceOrderId, order.items[i].sourceItemId, true, 'repeated-order');
                }
                
                resolve({sourceOrderId:  order.sourceOrderId, result:true, message:"order.isDownloaded==true."});
                return;
            }
            else{
                downloadOrderItems(order)
                .then((res)=>{
                    logWriter(order.sourceOrderId, "-", "DLOrderItems", "SUCCESS: "+res.message);
                    resolve({sourceOrderId: order.sourceOrderId, result:true, message:"SUCCESS downloadOrderItems"});
                    return;
                })
                .catch((err)=>{
                    logWriter(order.sourceOrderId, "-", "DLOrderItems", "ERROR: "+err.message);
                    resolve({sourceOrderId: order.sourceOrderId, result:false, message:"ERROR downloadOrderItems: "+ err.message});
                    return;
                        
                })
            }
        });
    });
}





function downloadOrderItems(order)
{
    return new Promise(function (resolve, reject) {
    logWriter(order.sourceOrderId, '-', "DLOrderItems", "START");

    var downloadDir=dltempPath;
    var filesForDownload = [];
    for (var i = 0; i < order.items.length; i++) {
        logWriter(order.sourceOrderId, order.items[i].sourceItemId, "DLOrderItems", "PUSH ITEM: "+i.toString()+", "+order.items[i].sourceItemId);
        filesForDownload.push(downloadOneSn(downloadDir, order.sourceOrderId, order.items[i], false));
    }

    Promise.all(filesForDownload).then(function(downloadResults) {
        var dlErrorCnt=0;
        for(var i=0; i<downloadResults.length; i++)
        {
            var res=downloadResults[i];
            if(res.result==true)
                logWriter(res.sourceOrderId, res.sourceItemId, "DLResult", "SUCCESS:"+ res.message);
            else{
                dlErrorCnt++;
                logWriter(res.sourceOrderId, res.sourceItemId, "DLResult", "FAIL:"+ res.message);
            }
        }

        if(dlErrorCnt==0)
        {
            //move from temp to print folder, & send complete msg
            
            var filesForMove=[];
            for(var i=0;i<downloadResults.length;i++){
                var res=downloadResults[i];
                var source=res.file;
                var des=res.file.replace(dltempPath, dlprintPath);
                filesForMove.push(moveFile(res.sourceOrderId, res.sourceItemId, source, des));
            }

            Promise.all(filesForMove).then(function(moveResults) {
                var mvErrorCnt=0;
                for(var i=0; i<downloadResults.length; i++)
                {
                    var res=moveResults[i];
                    if(res.result==true){
                        kafkaProducer_StatusDownloadCompele(res.sourceOrderId, res.sourceItemId, true, res.message);
                        logWriter(res.sourceOrderId, res.sourceItemId, "MvResult", "SUCCESS:"+ res.message);
                    }
                    else{
                        mvErrorCnt++;
                        logWriter(res.sourceOrderId, res.sourceItemId, "MvResult", "FAIL:"+ res.message);
                    }
                }

                if(mvErrorCnt==0){

                    //kafkaProducer(kafkaProducerTopic_downloadComplete, res.sourceOrderId, "", true, res.message);    
                    var query = {"sourceOrderId":res.sourceOrderId},
                    update = {"isDownloaded": true};
                    Order.updateOne(query, update, function (err, result) {
                        if(err)
                            logWriter(res.sourceOrderId, '-', "Order.updateOne", "Error: updateOne(downloadOrderItem:ALL COMPLETE): "+err);
                    });
    
                    resolve({sourceOrderId: order.sourceOrderId, result:true, message:"SUCECSS: files moved to print. "+moveResults.length.toString()});
                    return;
                }
                else{
                    reject({sourceOrderId: order.sourceOrderId, result:false, message:"FAIL: move file mvErrorCnt="+mvErrorCnt.toString()});
                    return;
                }
            });
                   
        }else{
            reject({sourceOrderId: order.sourceOrderId, result:false, message:"FAIL: download dlErrorCnt="+dlErrorCnt.toString()});
            return;
        }

    });
});
}

function moveFile(sourceOrderId, sourceItemId,source, des)
{
    return new Promise(function (resolve, reject) {

        logWriter(sourceOrderId, sourceItemId, "moveFile", "des: "+des);
        mv(source, des, {mkdirp:true},function(err){
            if(err)
                resolve({sourceOrderId: sourceOrderId,sourceItemId: sourceItemId, result:false, message:"ERROR: moveFile. "+err+" "+des});
            else{
                var logdatetime = logdate(new Date());
                var dt = logdatetime[2];
                logWriter("mvToPrint-", sourceItemId, sourceOrderId, "INSERT INTO #sn(dt,fid,machine,ordernumber,sn,filePath) values('20"+dt+"',"+facilityId+",'"+os.hostname+"','"+sourceOrderId+"','"+sourceItemId+"','"+des+"')");
                resolve({sourceOrderId: sourceOrderId,sourceItemId: sourceItemId, result:true, message: "SUCECSS: moveFile. "+des});
            }
        });
    
    });
}

function kafkaProducer_StatusDownloadCompele(sourceOrderId, sourceItemId, result, message)
{
    kafkaProducer(kafkaProducerTopic_downloadComplete, sourceOrderId, sourceItemId, true, message);
}

module.exports = downloadOneOrder;