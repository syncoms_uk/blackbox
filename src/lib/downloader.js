'use strict';
//
// Set folder names
const
    rootFolder = process.cwd() + "/";
var src = rootFolder + 'src/',
    lib = rootFolder + "src/lib/",
    models = rootFolder + "src/models/";
   

// downloader settings
var config = require(src + "config");
var dlParallel = config.get("downloader:parallel");


// Load packages
var
    logWriter = require(lib + "logWriter"),
    logdate = require(lib + "logdate"),
    downloadOneOrder = require(lib + "downloadOneOrder");
    

//
//Load models
var Order = require(models + "order");


function downloader(){
    logWriter("-", "-", "downloader", "--START--");
    var query ={"isDownloaded": false};
    Order.find(query, function (err, docs) {
        if (err) throw err;   
        logWriter("-", "-", "downloader", "Before: docs.length: "+docs.length.toString());
        downloadOrders(docs, dlParallel, function(){
            logWriter("-", "-", "DlOrders", "onCompleteLoop");
            setTimeout(function(){ downloader(); }, (60*60*1000));
        });
    });
}


function downloadOrders(orders, size,  onCompleteLoop)
{
    function go(i){
        if(i*size>=orders.length){
            onCompleteLoop();
        }
        else{
            logWriter('-', '-', "DlOrders", "orders.length="+orders.length.toString()+", size:"+size.toString()+"---------------------------------");

            var awaitingOrders = [];
            for (var j = i*size; (j < (i+1)*size  && j< orders.length); j++) {
                logWriter('-', '-', "DlOrders", "PUSH ORDER: "+j.toString()+", "+orders[j].sourceOrderId +', timeStamp: '+orders[j].timestamp);
                awaitingOrders.push(downloadOneOrder(orders[j].sourceOrderId));
                //awaitingOrders.push(downloadOneOrder(orders[orders.length-j-1].sourceOrderId));
            }

            Promise.all(awaitingOrders).then(function(values) {
                var failCnt=0;
                for(var k=0; k<values.length; k++)
                {
                    var res=values[k];
                    if(res.result==true)
                        logWriter(res.sourceOrderId, '-', "DLOneOrder", "SUCCESS Promise.all:"+ res.message);
                    else{
                        failCnt++;
                        logWriter(res.sourceOrderId, '-', "DLOneOrder", "FAIL Promise.all:"+ res.message);
                    }
                }
                
                return go(i+1);
            });
            
        }
    }
    go(0);
}

module.exports = downloader;
