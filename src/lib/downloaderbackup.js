'use strict';
//
// Set folder names
const
    rootFolder = process.cwd() + "/";
var src = rootFolder + 'src/',
    lib = rootFolder + "src/lib/",
    models = rootFolder + "src/models/";
   

// downloader settings
var os = require('os');
var mv = require("mv");
var config = require(src + "config");
var facilityId = config.get("facility:id");
var dltempPath = config.get("downloader:tempPath");
var dlprintPath = config.get("downloader:printPath");
var dlretryInterval1 = config.get("downloader:retryIntervalSeconds_1"); 
var dlretryInterval2 = config.get("downloader:retryIntervalSeconds_2"); 
var dlretryInterval3 = config.get("downloader:retryIntervalSeconds_3");
var mwareTimeout = config.get("downloader:mwareTimeout");
var dlParallel = config.get("downloader:parallel");


//var dlSetting = {tempPath: dltempPath, printPath:dlprintPath, retryInterval1: dlretryInterval1, retryInterval2:dlretryInterval2, retryInterval3:dlretryInterval3};


var kafkaProducerTopic_downloadFile = config.get("kafkaProducer-status:topic-download-file");
var kafkaProducerTopic_downloadComplete= config.get("kafkaProducer-status:topic-download-complete");
//
// Load packages
var
    async = require('async'),
    //convertPdf2Png = require(lib + "convertPdf2Png"),
    download = require('image-downloader'),
    fileExists = require('file-exists'),
    kafka = require("kafka-node"),
    makeDir = require('make-dir'),
    path = require("path"),
    kafkaProducer = require(lib + "kafkaProducer"),
    logWriter = require(lib + "logWriter"),
    logdate = require(lib + "logdate");
    

//
//Load models
var Order = require(models + "order");



function downloader(){
    logWriter("-", "-", "downloader", "--START--");
    var query ={"isDownloaded": false};
    Order.find(query, function (err, docs) {
        if (err) throw err;   
        logWriter("-", "-", "downloader", "Before: docs.length: "+docs.length.toString());
        downloadOrders(docs, dlParallel, function(){
            logWriter("-", "-", "DlOrders", "onCompleteLoop");
            setTimeout(function(){ downloader(); }, (120*60*1000));
        });
    });
}


function downloadOrders(orders, size,  onCompleteLoop)
{
    function go(i){
        if(i*size>=orders.length){
            onCompleteLoop();
        }
        else{
            logWriter('-', '-', "DlOrders", "orders.length="+orders.length.toString()+", size:"+size.toString()+"---------------------------------");

            var awaitingOrders = [];
            for (var j = i*size; (j < (i+1)*size  && j< orders.length); j++) {
                logWriter('-', '-', "DlOrders", "PUSH ORDER: "+j.toString()+", "+orders[j].sourceOrderId);
                awaitingOrders.push(downloadOneOrder(orders[j].sourceOrderId));
            }

            Promise.all(awaitingOrders).then(function(values) {
                var failCnt=0;
                for(var k=0; k<values.length; k++)
                {
                    var res=values[k];
                    if(res.result==true)
                        logWriter(res.sourceOrderId, '-', "DLOneOrder", "SUCCESS Promise.all:"+ res.message);
                    else{
                        failCnt++;
                        logWriter(res.sourceOrderId, '-', "DLOneOrder", "FAIL Promise.all:"+ res.message);
                    }
                }
                
                return go(i+1);
            });
            
        }
    }
    go(0);
}

function downloadOneOrder(sourceOrderId){
    return new Promise(function (resolve, reject) {
        var query = {"sourceOrderId": sourceOrderId};
        var orderExists=false;
        Order.findOne(query, function(err, order) { 
            if(err)
                logWriter(t.sourceOrderId, '-', "downloadOneOrder", "Error (Order.FindOne): " + err);

            if(order==null){
                    resolve({sourceOrderId: "null", result:false, message:"order==null"});
                    return;
            }
            if(order.isDownloaded){
                resolve({sourceOrderId:  order.sourceOrderId, result:true, message:"order.isDownloaded==true."});
                return;
            }
            else{
                downloadOrderItems(order)
                .then((res)=>{
                    logWriter(order.sourceOrderId, "-", "DLOrderItems", "SUCCESS: "+res.message);
                    resolve({sourceOrderId: order.sourceOrderId, result:true, message:"SUCCESS downloadOrderItems"});
                    return;
                })
                .catch((err)=>{
                    logWriter(order.sourceOrderId, "-", "DLOrderItems", "ERROR: "+err.message);
                    resolve({sourceOrderId: order.sourceOrderId, result:false, message:"ERROR downloadOrderItems: "+ err.message});
                    return;
                        
                })
            }
        });
    });
}





function downloadOrderItems(order)
{
    return new Promise(function (resolve, reject) {
    logWriter(order.sourceOrderId, '-', "DLOrderItems", "START");

    var downloadDir=dltempPath;
    var filesForDownload = [];
    for (var i = 0; i < order.items.length; i++) {
        logWriter(order.sourceOrderId, order.items[i].sourceItemId, "DLOrderItems", "PUSH ITEM: "+i.toString()+", "+order.items[i].sourceItemId);
        filesForDownload.push(downloadOrderItem(downloadDir, order.sourceOrderId, order.items[i], i));
    }

    Promise.all(filesForDownload).then(function(downloadResults) {
        var dlErrorCnt=0;
        for(var i=0; i<downloadResults.length; i++)
        {
            var res=downloadResults[i];
            if(res.result==true)
                logWriter(res.sourceOrderId, res.sourceItemId, "DLResult", "SUCCESS:"+ res.message);
            else{
                dlErrorCnt++;
                logWriter(res.sourceOrderId, res.sourceItemId, "DLResult", "FAIL:"+ res.message);
            }
        }

        if(dlErrorCnt==0)
        {
            //move from temp to print folder, & send complete msg
            
            var filesForMove=[];
            for(var i=0;i<downloadResults.length;i++){
                var res=downloadResults[i];
                var source=res.file;
                var des=res.file.replace(dltempPath, dlprintPath);
                filesForMove.push(moveFile(res.sourceOrderId, res.sourceItemId, source, des));
            }

            Promise.all(filesForMove).then(function(moveResults) {
                var mvErrorCnt=0;
                for(var i=0; i<downloadResults.length; i++)
                {
                    var res=moveResults[i];
                    if(res.result==true)
                        logWriter(res.sourceOrderId, res.sourceItemId, "MvResult", "SUCCESS:"+ res.message);
                    else{
                        mvErrorCnt++;
                        logWriter(res.sourceOrderId, res.sourceItemId, "MvResult", "FAIL:"+ res.message);
                    }
                }

                if(mvErrorCnt==0){
                    logWriter(res.sourceOrderId, '-', "MvToPrintFol", "SUCCESS: "+ kafkaProducerTopic_downloadComplete +": "+res.message);
                    kafkaProducer(kafkaProducerTopic_downloadComplete, res.sourceOrderId, "", true, res.message);
    
                    var query = {"sourceOrderId":res.sourceOrderId},
                    update = {"isDownloaded": true};
                    Order.updateOne(query, update, function (err, result) {
                        if(err)
                            logWriter(res.sourceOrderId, '-', "Order.updateOne", "Error: updateOne(downloadOrderItem:ALL COMPLETE): "+err);
                    });
    
                    resolve({sourceOrderId: order.sourceOrderId, result:true, message:"SUCECSS: files moved to print. "+moveResults.length.toString()});
                    return;
                }
                else{
                    reject({sourceOrderId: order.sourceOrderId, result:false, message:"FAIL: move file mvErrorCnt="+mvErrorCnt.toString()});
                    return;
                }
            });
                   
        }else{
            reject({sourceOrderId: order.sourceOrderId, result:false, message:"FAIL: download dlErrorCnt="+dlErrorCnt.toString()});
            return;
        }

    });
});
}

function moveFile(sourceOrderId, sourceItemId,source, des)
{
    return new Promise(function (resolve, reject) {
        logWriter(sourceOrderId, sourceItemId, "moveFile", "des: "+des);
        mv(source, des, {mkdirp:true},function(err){
            if(err)
                resolve({sourceOrderId: sourceOrderId,sourceItemId: sourceItemId, result:false, message:"ERROR: moveFile. "+err+" "+des});
            else{
                var logdatetime = logdate(new Date());
                var dt = logdatetime[2];
                logWriter("mvToPrint-", sourceItemId, sourceOrderId, "INSERT INTO #sn(dt,fid,machine,ordernumber,sn,filePath) values('20"+dt+"',"+facilityId+",'"+os.hostname+"','"+sourceOrderId+"','"+sourceItemId+"','"+des+"')");
                resolve({sourceOrderId: sourceOrderId,sourceItemId: sourceItemId, result:true, message: "SUCECSS: moveFile. "+des});
            }
        });
    });
}


function downloadOrderItem(downloadDir, sourceOrderId, item, i)
{
    return new Promise(function (resolve, reject) {
        logWriter(sourceOrderId, item.sourceItemId, "DLOrderItem", "START-- "+item.createdAt.toString()+"--"+downloadDir+"--"+i.toString());
        const targetPdfDir = path.join(downloadDir, item.sku.replace(/\|/g, "_"));       
        const targetPdfPath = path.join(targetPdfDir, getPdfFileName(sourceOrderId, item));
        
        const url = item.components[0].path;

        fileExists(targetPdfPath)
        .then(exists => {
            if(exists){
                logWriter(sourceOrderId, item.sourceItemId, "DLOrderItem", "FS SUGGESTED SKIP DOWNLOAD!!!!");
                var res = {"sourceOrderId":sourceOrderId, "sourceItemId": item.sourceItemId, result: true, file: targetPdfPath,"message": "SKIP DOWNLOAD: Print file Exists. "+targetPdfPath}
                UpdateOneItem(res);
                resolve(res);
                return;
            }
            else{
                makeDir(targetPdfDir)
                .then((targetPdfDir) => {
                    downloadImage(sourceOrderId, item, url, targetPdfPath)
                    .then((res)=>{
                        UpdateOneItem(res);
                        resolve(res);
                        return;
                    })
                    .catch((err)=>{
                        var res = {"sourceOrderId":sourceOrderId, "sourceItemId": item.sourceItemId, result: false, file: targetPdfPath,"message": "downloadImage:err: "+JSON.stringify(err)};
                        UpdateOneItem(res);
                        logWriter(sourceOrderId, item.sourceItemId, "DlImage", "Error: targetPdfPath: "+targetPdfPath+"; "+JSON.stringify(err));
                        resolve(res);
                        return;
                    });
                })
                .catch(err => {
                    var res = {"sourceOrderId":sourceOrderId, "sourceItemId": item.sourceItemId, result: false, file: targetPdfPath,"message": "makeDir:err:"+err};
                    UpdateOneItem(res);
                    logWriter(sourceOrderId, item.sourceItemId, "MakeDir", "Error: "+targetPdfPath+"; "+err);
                    resolve(res);
            });
            }
        });



    });
}

// Download to a directory and save with an another filename
function downloadImage(sourceOrderId, item, url, targetPdfPath) {

    var pdfUrl=url;
    if(facilityId=="16" || facilityId=="18" || facilityId=="19"){
    //if(item.sku.toLowerCase().indexOf("blanket")>=0){
        pdfUrl=url+"?partner=CHINA";
    }

    logWriter(sourceOrderId, item.sourceItemId, "DLImage", pdfUrl);
    //logWriter(sourceOrderId, item.sourceItemId, "DLImage", targetPdfPath);
    return new Promise(function (resolve, reject) {
    var options = {
        url: pdfUrl,
        dest: targetPdfPath, // Save to /path/to/dest/photo.jpg
        timeout: mwareTimeout //10*60*1000 10min
    }


    download.image(options)
    .then(({fileName,image}) => {

            if(fileName)
                logWriter(sourceOrderId, item.sourceItemId, "DLImage", "SUCCESS: "+fileName);


                //we dont have jobId here, hence cannot get preview
        //     var siteCode=item.sourceItemId.substring(0, 1);
        //     var prefix=item.jobId.substring(0, 4);
        //     var previewUrl ="http://storage.printerpix.com/MediaClip/thumb/"+siteCode+"/"+prefix+"/"+item.jobId+"/"+item.jobId+".jpg";
    
        //     var options_preview = {
        //         url: previewUrl,
        //         dest: targetPreviewPath // Save to /path/to/dest/photo.jpg
        //     }

        //     download.image(options_preview)
        //     .then(({fileName,image}) => {
        //             resolve({"sourceOrderId":sourceOrderId,"sourceItemId":item.sourceItemId, "result":true, file: targetPdfPath, "message":"download.image(print & preview): SUCCESS. "+targetPdfPath});                
        //             return;
        //     })
        //     .catch((err) => {
        //             var errmsg="download.image(preview): "+ err;
        //             reject({"sourceOrderId":sourceOrderId,"sourceItemId":item.sourceItemId, "result":false, "message":errmsg });
        //             return;
        //    });

            resolve({"sourceOrderId":sourceOrderId,"sourceItemId":item.sourceItemId, "result":true, file: targetPdfPath, "message":"download.image: SUCCESS. "+targetPdfPath});                
            return;
            
    })
    .catch((err) => {
            logWriter(sourceOrderId, item.sourceItemId, "DLImage", "ERROR: "+ err +", "+pdfUrl+", "+targetPdfPath);
            logWriter("DownloadErr-", item.sourceItemId, sourceOrderId, err+", "+pdfUrl+", "+targetPdfPath);
            var errmsg="download.image: "+err;
            reject({"sourceOrderId":sourceOrderId,"sourceItemId":item.sourceItemId, "result":false, "message":errmsg +"; timeout="+mwareTimeout.toString()+"; "+pdfUrl });
            return;
        });
            //resolve({sourceItemId:item.sourceItemId, result:false, message:"---"});
    });
}

function UpdateOneItem(res)
{
    var sourceOrderId=res.sourceOrderId;
    var sourceItemId = res.sourceItemId;
    var dlResult=(res.result==true? true: false);
    var message = res.message;
    var topic = kafkaProducerTopic_downloadFile;

                  
    var query = {"sourceOrderId":sourceOrderId,"items.sourceItemId": sourceItemId},
    update = {"items.$.isDownloaded": dlResult, "items.$.updatedAt":new Date()};
    Order.updateOne(query, update, function (err, result) {
        if(err)
            logWriter(sourceOrderId, sourceItemId, "UpdateOneItem", "Error UpdateOneItem(DLResult="+dlResult.toString()+"): "+err);
        else
            logWriter(sourceOrderId, sourceItemId, "UpdateOneItem", "SUCCESS UpdateOneItem(DLResult="+dlResult.toString()+")");
    });

    kafkaProducer(kafkaProducerTopic_downloadFile, sourceOrderId, sourceItemId, dlResult, message);  
}

function getPdfFileName(sourceOrderId, item)
{
    var splits = item.sku.split("|");
    var skuBase=splits[0];

    var pdfName = skuBase + "_"+ item.sourceItemId;

    var addOnItems=item.sku.replace(skuBase,'').replace(/\|/g, "_");    
    if(addOnItems.length>0)
        pdfName = pdfName + "_"+ addOnItems;

    return (pdfName+".pdf").replace("__","_");
}




function retryDecision(item) //NOT IN USE
{
    var diff = getSecondsBetweenDates(item.createdAt, item.updatedAt);
    if(diff==0)//fresh entry
        return true;
    var now = new Date();
    var secondsFromCreation = getSecondsBetweenDates(item.createdAt, now);
    var secondsFromLastAttempt = getSecondsBetweenDates(item.updatedAt, now);
    if(secondsFromCreation < 2*60*60)//first 2 hour, retry at dlretryInterval1
    {
        if(secondsFromLastAttempt < dlretryInterval1)
        {
            console.log("getRetryDecision(SKIP: secondsFromLastAttempt < dlretryInterval1): secondsFromLastAttempt:"+secondsFromLastAttempt.toString());
            return false;
        }
    }
    else if(secondsFromCreation< 3*24*60*60)//first 3 days, retry at dlretryInterval2
    {
        if(secondsFromLastAttempt < dlretryInterval2)
        {
            console.log("getRetryDecision(SKIP: secondsFromLastAttempt < dlretryInterval2): secondsFromLastAttempt:"+secondsFromLastAttempt.toString());
            return false;
        }
    }
    else //afterwards, retry at dlretryInterval3
    {
        if(secondsFromLastAttempt < dlretryInterval3)
        {
            console.log("getRetryDecision(SKIP: secondsFromLastAttempt < dlretryInterval3): secondsFromLastAttempt:"+secondsFromLastAttempt.toString());
            return false;
        }
    }

    return true;
}

function getSecondsBetweenDates(t1, t2) //NOT IN USE
{
    var dif = t1.getTime() - t2.getTime();    
    var Seconds_from_T1_to_T2 = dif / 1000;
    var Seconds_Between_Dates = Math.floor(Math.abs(Seconds_from_T1_to_T2));
    return Seconds_Between_Dates;
}

module.exports = downloader;
