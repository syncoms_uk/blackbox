'use strict';
//
var src = process.cwd() + "/src/";

var mongoose = require("mongoose");
// log = require(libs + "log")(module),
var config = require(src + "config");


mongoose.Promise = global.Promise;

var mongooseConnection = mongoose.connect(config.get("mongoose:uri"), {
        useNewUrlParser: true
    });

// var db = mongoose.connection;

// db.on("error", function (err) {
//     console.error("Connection error:", err.message);
//     // log.error('Connection error:', err.message);
// });

// db.once("open", function callback() {
//     console.info("Connected to DB!");
//     // log.info("Connected to DB!");
// });

module.exports = mongooseConnection;