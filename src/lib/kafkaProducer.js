'use strict';
const rootFolder = process.cwd() + "/";
var src = rootFolder + 'src/';
var lib = rootFolder + "src/lib/";
var logWriter = require(lib + "logWriter");

// KafkaConsumer settings
var config = require(src + "config");
var kafkaProducerHost = config.get("kafkaProducer-status:host");
var kafkaProducerAutoCommit = config.get("kafkaProducer-status:autoCommit");




var facilityId=config.get("facility:id");

var kafka = require('kafka-node');
var os = require('os');






var Client = new kafka.KafkaClient({kafkaHost: kafkaProducerHost}); 
var options = { autoCommit: kafkaProducerAutoCommit, fetchMaxWaitMs: 10000, fetchMaxBytes: 1024 * 1024 };
var producer = new kafka.Producer(Client, options);
producer.on('ready', function () {
    logWriter('KafkaPro-', '-', "OnReady", "");
    
});

producer.on('error', function (err) {
    logWriter('KafkaPro-', '-', "OnError", err);
  }); 



function kafkaProducer(topic, order, sn, result, note){
    var message = { sender:facilityId, machine: os.hostname(), order: order,sn: sn, result:result, note:note };
    var payloads = [{ topic: topic, messages: JSON.stringify(message), partition: 0 },];

     producer.send(payloads, function (err, data) {
        var msgDetail = "topic: " + payloads[0].topic +", sender: "+message.sender+", machine: "+message.machine+", result: "+message.result.toString();
        if(err){
            logWriter(order, sn, "KafkaSend", "ERROR: "+err+ ", " +msgDetail);
            logWriter('KafkaPro-', sn, order, "ERROR: "+topic+ ', '+err+', '+msgDetail);
            setTimeout(function(){kafkaProducer(topic, order, sn, result, note);}, (2*60*1000));
        }
        else{
            logWriter(order, sn, "KafkaSend", "SUCCESS: "+ JSON.stringify(data)+ ", "+ msgDetail);
            logWriter('KafkaPro-', sn, order, "SUCCESS: "+topic + ', ' + JSON.stringify(data)+". ");
        }
        }); 
    
}

module.exports = kafkaProducer;