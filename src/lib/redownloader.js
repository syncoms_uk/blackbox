'use strict';
//
// Set folder names
const
    rootFolder = process.cwd() + "/";
    
var src = rootFolder + 'src/',
    lib = rootFolder + "src/lib/",
    models = rootFolder + "src/models/";
   

// downloader settings
const fs = require('fs');
var os = require('os');
var mv = require("mv");
var config = require(src + "config");
var facilityId = config.get("facility:id");
var dltempPath = config.get("downloader:tempPath");
var dlprintPath = config.get("downloader:printPath");
var dlbadPath = config.get("downloader:badPath");
var mwareTimeout = config.get("downloader:mwareTimeout");
var dlParallel = config.get("downloader:parallel");

// Load packages
var
async = require('async'),
download = require('image-downloader'),
fileExists = require('file-exists'),
makeDir = require('make-dir'),
path = require("path"),
logWriter = require(lib + "logWriter"),
logdate = require(lib + "logdate"),
downloadOneSn = require(lib + "downloadOneSn");

var Order = require(models + "order");
const downloadDir = path.join(dlprintPath, '_FixedBadPdf');  

function redownloader(){
    logWriter("-", "-", "redownload", "--START--"+dlbadPath);
    //fs.readdirSync(dlbadPath).forEach(file => {console.log(file);});

    fs.readdir(dlbadPath, (err, files) => {
        if(files!=null){
        logWriter("-", "-", "redownload", "files.length:"+files.length.toString());
        downloadFiles(files, dlParallel, function(){
            logWriter("-", "-", "redownload", "onCompleteLoop");
            setTimeout(function(){redownloader();}, (60*60*1000));
        });
        }
      })


}

function downloadFiles(badFiles, size,  onCompleteLoop)
{
    function go(i){
        if(i*size>=badFiles.length){
            onCompleteLoop();
        }
        else{
            logWriter('-', '-', "ReDownload", "badFiles.length="+badFiles.length.toString()+", size:"+size.toString()+"------------------");

            var awaitingSns = [];
            for (var j = i*size; (j < (i+1)*size  && j< badFiles.length); j++) {
                logWriter('-', '-', "ReDownload", "PUSH SN: "+j.toString()+", "+badFiles[j]);
                awaitingSns.push(RedownloadOneSn(badFiles[j]));
            }

            Promise.all(awaitingSns).then(function(values) {
                var failCnt=0;
                for(var k=0; k<values.length; k++)
                {
                    var res=values[k];
                    if(res.result==true)
                        logWriter(res.sourceOrderId, '-', "ReDownload", "SUCCESS Promise.all:"+ res.message);                        
                    else{
                        failCnt++;
                        logWriter(res.sourceOrderId, '-', "ReDownload", "FAIL Promise.all:"+ res.message);
                    }
                }
                
                return go(i+1);
            });
            
        }
    }
    go(0);
}

function RedownloadOneSn(badFile)
{
    return new Promise(function (resolve, reject) {
        var sn = extractSerialNo(badFile);
        if(sn==''){
            resolve({"sourceOrderId":'-', "sourceItemId": sn, result: false, file: '',"message": "Unrecognised Sn "+badFile});
            return;
        }

        logWriter('', sn, "redownload", "extractSerialNo="+sn);
        var query = {'items':{'$elemMatch':{'sourceItemId': sn}}};
        Order.findOne(query, function(err,order) { 
        if(err){
            logWriter("-", "-", "redownload", "findOneError:"+sn+". "+err);
            resolve({"sourceOrderId":'-', "sourceItemId": sn, result: false, file: '',"message": "findOne ERROR. "+sn});
            return;
        }
        if(order){
            //console.log(JSON.stringify(order));
            for (var i = 0; i < order.items.length; i++) {
                if(order.items[i].sourceItemId.toString().toLowerCase()==sn.toLowerCase())
                {
                    
                    downloadOneSn(downloadDir, order.sourceOrderId, order.items[i], true)
                    .then((res)=>{
                        if(res.result==true){
                            logWriter(res.sourceOrderId, res.sourceItemId, "redownload", "SUCCESS ReDownload:"+ res.message);
                            var badPathFull=path.join(dlbadPath, badFile);  
                            fs.unlink(badPathFull, (err) => {
                                if(err)
                                    logWriter(res.sourceOrderId, res.sourceItemId, "redownload", "fs.unlink error:"+ err+ " "+badPathFull);
                                else 
                                    logWriter(res.sourceOrderId, res.sourceItemId, "redownload", 'SUCCESSFULLY DELETED '+badPathFull);
                            });
                        }
                        else
                            logWriter(res.sourceOrderId, res.sourceItemId, "redownload", "FAIL ReDownload:"+ res.message);

                        resolve(res);
                        return;
                    });
                }
            }
            
            }
            else
            {
                resolve({"sourceOrderId":order.sourceOrderId, "sourceItemId": sn, result: false, file: '',"message": "Invalid Sn. "+badFile});
                return;
            }
        });
       
    });
}

function extractSerialNo(input)
{
    var regex =/([a-zA-Z0-9]{8}\d{3}[a-zA-Z0-9]{1}[ctCT-]{1}\d{3})/;
    var sns=regex.exec(input);
    if(!sns)
        return ''

    if(sns.length==0)
        return '';
    else
        return sns[0].toUpperCase();
}

module.exports = redownloader;
