var fs = require('fs');


function logdate(currentDt)
{
    try{

        if(currentDt==null)
            currentDt = new Date();
        var d = (currentDt.getFullYear() % 100) + '-' + 
        (currentDt.getMonth()+1).toString().padStart(2, '0') + '-' + 
        currentDt.getDate().toString().padStart(2, '0');

        var t = currentDt.getHours().toString().padStart(2, '0') + ':'+
            currentDt.getMinutes().toString().padStart(2, '0') + ':'+
            currentDt.getSeconds().toString().padStart(2, '0');

        var dt = d+' '+t;
        return [d,t,dt];
    }
    catch(e){
        console.log("logdate Ex: "+ JSON.stringify(e));
    }
    return ["ex_d","ex_t","ex_dt"];
}

module.exports = logdate;