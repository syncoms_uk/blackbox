'use strict';

// Set folder names
const
    rootFolder = process.cwd() + "/";
var src = rootFolder + 'src/',
    lib = rootFolder + "src/lib/",
    models = rootFolder + "src/models/";
    
    
// KafkaConsumer settings
var config = require(src + "config");
var kchost = config.get("kafkaConsumer:host");
var kctopic = config.get("kafkaConsumer:topic");
var kcgroupdId = config.get("kafkaConsumer:groupdId");
var kcfromOffset = config.get("kafkaConsumer:fromOffset");
var kcautoCommit = config.get("kafkaConsumer:autoCommit");
var manualOffset = config.get("kafkaConsumer:offset");
var kcSetting = {host:kchost, topic:kctopic, groupId: kcgroupdId, fromOffset:kcfromOffset, autoCommit:kcautoCommit};


// Load packages
var
    kafka = require("kafka-node"),
    downloader = require(lib + "downloader"),
    logWriter = require(lib + "logWriter");

//Load models
var Order = require(models + "order");

// Kafka settings
const kafkaSettings = kcSetting;


//
// Main entry point: convertPdf2Png
//
function kafkaConsumer(){

    var client = new kafka.KafkaClient({kafkaHost: kafkaSettings.host});

    var offset = new kafka.Offset(client);
    var payloads = [ { topic: kctopic, partition: 0 } ];
    offset.fetchCommits(kcgroupdId, payloads, function (err, data) {
        

        var latestOffset = 0;
        if(data){
            latestOffset=data[kctopic]['0'];
            logWriter('kafkaCon-', 'offset', "fetchCommits", "latestOffset:" + latestOffset.toString());

        }
        else
            logWriter('kafkaCon-', 'offset', "fetchCommits", "data: NULL");

        if(latestOffset<manualOffset)
            latestOffset=manualOffset;

        if(latestOffset == -1)
        {
            
            Order.findOne({})
            .sort('-offset')  // give me the max
            .exec(function (err, order) {
                if(order!=null){
                    logWriter('kafkaCon-', 'offset', "GetFromDB", "order.offset: "+order.offset.toString());
                    if(latestOffset<order.offset)
                        latestOffset=order.offset;

                    getMessages(latestOffset);
                }
                else
                    getMessages(latestOffset);
            });
        }
        else
            getMessages(latestOffset);
    });
    
}

function getMessages(current_offset) {

    if(current_offset<manualOffset)
        current_offset=manualOffset;

    logWriter('kafkaCon-', current_offset.toString(), "GetMessages", "--START--");
    return new Promise(function (resolve, reject) {
        var client = new kafka.KafkaClient({kafkaHost: kafkaSettings.host});
        var payloads = [{
            topic: kafkaSettings.topic,
            offset: current_offset,
            partition: 0
        }];
        var options = {
            autoCommit: kafkaSettings.autoCommit,
            // fetchMaxWaitMs: 100, 
            // fetchMaxBytes: 1024 * 1024,
            fromOffset: kafkaSettings.fromOffset,
            groupId: kafkaSettings.groupId
        };
        logWriter('kafkaCon-', current_offset.toString(), "GetMessages", "kafkaHost: "+kafkaSettings.host);
        logWriter('kafkaCon-', current_offset.toString(), "GetMessages", "payloads: "+JSON.stringify(payloads));
        logWriter('kafkaCon-', current_offset.toString(), "GetMessages", "options: " + JSON.stringify(options));


        // Start Kafka consumer
        var consumer = new kafka.Consumer(client, payloads, options);

        // Got data
        var index = 0;
        consumer.on('message', function (message) {
            try{
                //console.log("message: " + JSON.stringify(message));
                var t = JSON.parse(message.value);
                setItemTimeStamp(t);
                //console.log(t);
                logWriter(t.sourceOrderId, '-', "OnMessage", "Offset: " + message.offset);
            
                var unixtimestamp=Math.floor((new Date()).getTime() / 1000);
                var query = {"sourceOrderId": t.sourceOrderId};
                var update = {
                    _id: t.sourceOrderId,
                    timestamp: message.timestamp,
                    offset: message.offset,
                    timestamp_created: unixtimestamp,
                    timestamp_retry: unixtimestamp,
                    attributes: t.attributes,
                    sourceOrderId: t.sourceOrderId,
                    slaTimestamp: t.slaTimestamp,
                    items: t.items,
                    shipments: t.shipments,
                };
                var options = { upsert: true, new: true, overwrite: true, setDefaultsOnInsert: true };

                var orderExists=false;
                Order.findOne(query, function(err,doc) { 
                    if(err)
                        logWriter(t.sourceOrderId, '-', "Order.FindOne", "Error: " + err);
                    
                    if(doc){
                        logWriter(t.sourceOrderId, '-', "Order.FindOne", "SUCCESS: " + doc.sourceOrderId +", "+doc.createdAt.toString()+", isDownloaded:"+ doc.isDownloaded.toString());
                        logWriter(t.sourceOrderId, '-', "Order.FindOne", "SUCCESS: ---SKIP INSERT---");
                        consumer.commit((error, data) => {
                            if(error)
                                logWriter('KafkaCon-', doc.sourceOrderId, 'OnMessage' , 'ERR: consumer.commit('+doc.offset.toString()+')(error): '+error);
                            if(data)
                                logWriter('KafkaCon-', doc.sourceOrderId, "OnMessage", "DATA: commit("+doc.offset.toString()+")(data):  "+JSON.stringify(data));
                            // Here: data == "Commit not needed"
                        });
                    }
                    else
                    {
                        Order.findOneAndUpdate(query, update, options, function(error, doc) {
                            if (error) {
                                logWriter(t.sourceOrderId, '-', "OnMessage", "findOneAndUpdate Err: " + error);
                                return;
                            }
                    
                            if (doc) 
                            {
                                consumer.commit((error, data) => {
                                    if(error)
                                        logWriter('KafkaCon-', doc.sourceOrderId, 'OnMessage' , 'ERR: consumer.commit('+doc.offset.toString()+')(error): '+error);
                                    if(data)
                                        logWriter('KafkaCon-', doc.sourceOrderId, "OnMessage", "DATA: commit("+doc.offset.toString()+")(data):  "+JSON.stringify(data));
    
                                    // Here: data == "Commit not needed"
                                });
                        
                                //update messaging offset
                                //downloader(doc);
                            } 
                        });
                    }

                });

            }catch(e) {
                logWriter('KafkaCon-', '-', "OnMessage", "Ex:  "+JSON.stringify(e));

            } // try

            index++;
        }); // consumer.on

        // Error
        consumer.on('error', function (err) {
            if (typeof err === 'string' || err instanceof String)
                logWriter('KafkaCon-', '-', "OnError", err);
            else
                logWriter('KafkaCon-', '-', "OnError", JSON.stringify(err));

        }); // consumer.on

        resolve();
    });
}

function setItemTimeStamp(order)
{
    var now = new Date();

    var i, n = order.items.length;
    for (i = 0; i < n; ++i) {
        order.items[i].createdAt = now;
        order.items[i].updatedAt = now;
    }
}


module.exports = kafkaConsumer;