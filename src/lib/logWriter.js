const rootFolder = process.cwd() + "/";
var lib = rootFolder + "src/lib/";
var logdate = require(lib + "logdate");
var fs = require('fs');

function logWriter(orderId12, sn16, activity16, details)
{
    try{
    if(orderId12.length>12)
        orderId12=orderId12.substring(0,12);

    if(sn16.length>16)
        sn16=sn16.substring(0,16);

    if(activity16.length>16)
        activity16=activity16.substring(0,16);

        var logdatetime = logdate(new Date());
        var d = logdatetime[0];
        var dt = logdatetime[2];

    var msg = dt.padEnd(17,' ') + "|"+
    orderId12.padEnd(12,' ') + "|"+
    sn16.padEnd(16,' ') + "|"+
    activity16.padEnd(16,' ') + "|"+
    details;

    if(details!=null && details.indexOf("INSERT INTO")<0)
        console.log(msg);

    //var newlineCharacter = "\n";
    var allLogStream = fs.createWriteStream("log\\"+d+'_all.txt', {'flags': 'a'});
    allLogStream.end(msg + '\r\n');

    //fs.appendFile("log\\"+d+'.txt', msg, function (err) {
    //    if (err) throw err;
    //  });

    if(orderId12.indexOf("-")<0){
        var logStream = fs.createWriteStream("log\\"+orderId12+'.txt', {'flags': 'a'});
        // use {'flag': 'a'} to append and {'flag': 'w'} to erase and write a new file
        logStream.end(msg+ '\r\n');
    }
    else
    {
        var logName=orderId12.replace('-','');

        if(orderId12.indexOf("mvToPrint-")>-1)
            msg=details;

        if(logName!=''){
            var logStream = fs.createWriteStream("log\\"+d+'_'+logName+'.txt', {'flags': 'a'});
            logStream.end(msg+ '\r\n');
        }
    }

    
    //allLogStream.end();
    }
    catch(e){
        console.log("logWriter Ex: "+ e);
    }
}

module.exports = logWriter;