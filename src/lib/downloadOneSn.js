'use strict';
//
// Set folder names
const
    rootFolder = process.cwd() + "/";
var src = rootFolder + 'src/',
    lib = rootFolder + "src/lib/",
    models = rootFolder + "src/models/";
   
var os = require('os');
var mv = require("mv");

    
// downloader settings
var config = require(src + "config");
var facilityId = config.get("facility:id");
var dltempPath = config.get("downloader:tempPath");
var dlprintPath = config.get("downloader:printPath");
var dlretryInterval1 = config.get("downloader:retryIntervalSeconds_1"); 
var dlretryInterval2 = config.get("downloader:retryIntervalSeconds_2"); 
var dlretryInterval3 = config.get("downloader:retryIntervalSeconds_3");
var mwareTimeout = config.get("downloader:mwareTimeout");

var kafkaProducerTopic_downloadFile = config.get("kafkaProducer-status:topic-download-file");

//
// Load packages
//
var os = require('os'),
mv = require("mv"),
download = require('image-downloader'),
fileExists = require('file-exists'),
kafka = require("kafka-node"),
makeDir = require('make-dir'),
path = require("path"),
kafkaProducer = require(lib + "kafkaProducer"),
logWriter = require(lib + "logWriter"),
logdate = require(lib + "logdate");

    

//
//Load models
var Order = require(models + "order");



function downloadOneSn(downloadDir, sourceOrderId, item, isRedownload)
{
    return new Promise(function (resolve, reject) {
        logWriter(sourceOrderId, item.sourceItemId, "DLOrderItem", "START-- "+item.createdAt.toString()+"--"+downloadDir+"--"+isRedownload.toString());
        const targetPdfDir = path.join(downloadDir, item.sku.replace(/\|/g, "_"));       
        const targetPdfPath = path.join(targetPdfDir, getPdfFileName(sourceOrderId, item));
        
        const url = item.components[0].path;

        fileExists(targetPdfPath)
        .then(exists => {
            if(exists){
                logWriter(sourceOrderId, item.sourceItemId, "DLOrderItem", "FS SUGGESTED SKIP DOWNLOAD!!!!");
                var res = {"sourceOrderId":sourceOrderId, "sourceItemId": item.sourceItemId, result: true, file: targetPdfPath,"message": "SKIP DOWNLOAD: Print file Exists. "+targetPdfPath}
                UpdateOneItem(res, isRedownload);
                resolve(res);
                return;
            }
            else{
                makeDir(targetPdfDir)
                .then((targetPdfDir) => {
                    downloadImage(sourceOrderId, item, url, targetPdfPath, isRedownload)
                    .then((res)=>{
                        UpdateOneItem(res, isRedownload);
                        resolve(res);
                        return;
                    })
                    .catch((err)=>{
                        var res = {"sourceOrderId":sourceOrderId, "sourceItemId": item.sourceItemId, result: false, file: targetPdfPath,"message": "downloadImage:err: "+JSON.stringify(err)};
                        UpdateOneItem(res, isRedownload);
                        logWriter(sourceOrderId, item.sourceItemId, "DlImage", "Error: targetPdfPath: "+targetPdfPath+"; "+JSON.stringify(err));
                        resolve(res);
                        return;
                    });
                })
                .catch(err => {
                    var res = {"sourceOrderId":sourceOrderId, "sourceItemId": item.sourceItemId, result: false, file: targetPdfPath,"message": "makeDir:err:"+err};
                    UpdateOneItem(res, isRedownload);
                    logWriter(sourceOrderId, item.sourceItemId, "MakeDir", "Error: "+targetPdfPath+"; "+err);
                    resolve(res);
            });
            }
        });



    });
}

// Download to a directory and save with an another filename
function downloadImage(sourceOrderId, item, url, targetPdfPath, isRedownload) {

    var pdfUrl=url;
    if(!isRedownload && (facilityId=="22")){pdfUrl="http://storage.printerpix.co.uk/Facility/"+item.sourceItemId+".pdf"}
    else if(facilityId!="17" && facilityId!="15"){
        pdfUrl=url+"?partner=CHINA";
    }

    if(facilityId!="17" && facilityId!="15"){
        pdfUrl=url+"?partner=CHINA";
    }

    logWriter(sourceOrderId, item.sourceItemId, "DLImage", pdfUrl);
    //logWriter(sourceOrderId, item.sourceItemId, "DLImage", targetPdfPath);
    return new Promise(function (resolve, reject) {
    var options = {
        url: pdfUrl,
        dest: targetPdfPath, // Save to /path/to/dest/photo.jpg
        timeout: mwareTimeout //10*60*1000 10min
    }


    download.image(options)
    .then(({fileName,image}) => {

            if(fileName)
                logWriter(sourceOrderId, item.sourceItemId, "DLImage", "SUCCESS: "+fileName);


            resolve({"sourceOrderId":sourceOrderId,"sourceItemId":item.sourceItemId, "result":true, file: targetPdfPath, "message":"download.image: SUCCESS. "+targetPdfPath});                
            return;
            
    })
    .catch((err) => {
            logWriter(sourceOrderId, item.sourceItemId, "DLImage", "ERROR: "+ err +", "+pdfUrl+", "+targetPdfPath);
            logWriter("DownloadErr-", item.sourceItemId, sourceOrderId, err+", "+pdfUrl+", "+targetPdfPath);
            var errmsg="download.image: "+err;
            reject({"sourceOrderId":sourceOrderId,"sourceItemId":item.sourceItemId, "result":false, "message":errmsg +"; timeout="+mwareTimeout.toString()+"; "+pdfUrl });
            return;
        });
            //resolve({sourceItemId:item.sourceItemId, result:false, message:"---"});
    });
}

function UpdateOneItem(res, isRedownload)
{
    var sourceOrderId=res.sourceOrderId;
    var sourceItemId = res.sourceItemId;
    var dlResult=(res.result==true? true: false);
    var message = res.message + "[isRedownload="+isRedownload.toString()+"]";
    var topic = kafkaProducerTopic_downloadFile;

    if(!isRedownload){             
    var query = {"sourceOrderId":sourceOrderId,"items.sourceItemId": sourceItemId},
    update = {"items.$.isDownloaded": dlResult, "items.$.updatedAt":new Date()};
    Order.updateOne(query, update, function (err, result) {
        if(err)
            logWriter(sourceOrderId, sourceItemId, "UpdateOneItem", "Error UpdateOneItem(DLResult="+dlResult.toString()+"): "+err);
        else
            logWriter(sourceOrderId, sourceItemId, "UpdateOneItem", "SUCCESS UpdateOneItem(DLResult="+dlResult.toString()+")");
    });
    }

    kafkaProducer(kafkaProducerTopic_downloadFile, sourceOrderId, sourceItemId, dlResult, message);  
}

function getPdfFileName(sourceOrderId, item)
{
    var splits = item.sku.split("|");
    var skuBase=splits[0];

    var pdfName = skuBase + "_"+ item.sourceItemId;

    var addOnItems=item.sku.replace(skuBase,'').replace(/\|/g, "_");    
    if(addOnItems.length>0)
        pdfName = pdfName + "_"+ addOnItems;

    return (pdfName+".pdf").replace("__","_");
}

module.exports = downloadOneSn;