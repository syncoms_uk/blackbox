var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ObjectId = require('mongoose').Types.ObjectId;


var OrderSchema = new Schema({
    _id: String,
    timestamp: Date,
    offset: Number,
    partition: Number,
    highWaterOffset: Number,
    scanned: {
        type: Number,
        default: 0
    },
    isDownloaded: {
        type: Boolean,
        default: false
    },
    attributes: [],
    sourceOrderId: String,
    slaTimestamp: Date,
    items: [{
        _id: false,
        barcode: String,
        isDownloaded: {
            type: Boolean,
            default: false
        },
        createdAt: Date, 
        updatedAt: Date, 
        shipmentIndex: Number,
        sourceItemId: String,
        sku: String,
        quantity: Number,
        unitWeight: Number,
        components: [{
            _id: false,
            path: String,
            fetch: Boolean,
            localFile: Boolean,
            barcode: String,
            attributes: {
                ProductFinishedPageSize: String,
                Size: String,
                "Size For Impo": String,
                Pages: Number,
                folding: String,
                Country: String,
                Substrate: String,
                StockCoverType: String,
                CoverType: String,
                Ribbon: String,
                code: String,
                weight: String,
            },
            code: String,
        }]
    }],
    shipments: [{
        _id: false,
        shipmentIndex: Number,
        shipTo: [{
            _id: false,
            name: String,
            companyName: String,
            address1: String,
            address2: String,
            town: String,
            postcode: String,
            state: String,
            isoCountry: String,
            country: String,
            phone: String,
        }],
        shipByDate: Date,
        canShipEarly: Boolean,
        carrier: [{
            _id: false,
            alias: String
        }],
        slaDays: Number,
    }]
}, {
    timestamps: true
})

// Export function to create model class
module.exports = mongoose.model("Order", OrderSchema);