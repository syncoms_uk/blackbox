var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ConfigSchema = new Schema({
    created: Date,
    lastModified: Date,
    currentOffset: Number,
    artworkFolder: String,
    imagesFolder: String,
    messaging : {
        host : String,
        topic : String,
        groupId : String,
        autoCommit : Boolean,
        fromOffset : Boolean
    },
    retry: {
        number: Number,
        interval: Number,
    },
});

// Export function to create model class
module.exports = mongoose.model("Config", ConfigSchema);